import Vue from 'vue'
import VueRouter from 'vue-router'
import StartScreen from './views/StartScreen.vue'
import QuestionScreen from './views/QuestionScreen.vue'
import ResultScreen from './views/ResultScreen.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        name: "StartScreen",
        component: StartScreen
    },
    {
        path: "/questions",
        name: "QuestionScreen",
        component: QuestionScreen
    },
    {
        path: "/results",
        name: "ResultScreen",
        component: ResultScreen
    }
]

const router = new VueRouter({routes})
export default router