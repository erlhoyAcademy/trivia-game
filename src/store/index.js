import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        questionQuantity: 1,
        selectedCategory: 9,
        difficulty: "easy",
        questions: [],
        questionIndex: 0,
        questionsFinishedLoading: false,
        results: [],
        score: 0
    },
    mutations: {
        setQuestionQuantity: (state, payload) => {
            state.questionQuantity = payload
        },
        setSelectedCategory: (state, payload) => {
            state.selectedCategory = payload
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setQuestionsFinishedLoading: (state, payload) => {
            state.questionsFinishedLoading = payload
        },
        increaseQuestionIndex(state) {

            if (state.questionIndex < state.questionQuantity - 1) {
                state.questionIndex += 1
            } 
            else {
                router.push("/results")
            }
        },
        resetQuestionIndex(state) {
            state.questionIndex = 0
        },
        pushUserAnswerToResults(state, payload) {
            state.results.push(payload)
        },
        resetResults(state) {
            state.results = []
        },
        resetScore(state) {
            state.score = 0
        },
        increaseScore(state) {
            state.score += 10
        }

    },
    actions: {
        
        async setQuestions({ commit }) {

            // array of numbers from 0 to n in random order, where n is the question quantity
            let randomOrderValues = []
            for (let i = 0; i < this.state.questionQuantity; i++) {
                randomOrderValues.push(i)
            }
            randomOrderValues = randomOrderValues.sort(() => Math.random() - 0.5)


            // questions is an array of objects
            const url = `https://opentdb.com/api.php?amount=${this.state.questionQuantity}&category=${this.state.selectedCategory}&difficulty=${this.state.difficulty}`
            let questions = await fetch(url).then(r => r.json()).then(res => res.results)

            const questionsRandomOrder = []
            randomOrderValues.forEach(value => {
                questionsRandomOrder.push(questions[value])
            })

            commit("setQuestions", questionsRandomOrder)
        }
    },
    getters: {

        // returning an array of the answers in random sorted order
        getAnswers(state) {
            // dummyArray used to randomly sort the answers
            let dummyArray = [0, 1]
            let answers = []
            let answersRandomOrder = []
            let currentQuestion = state.questions[state.questionIndex]

            switch (currentQuestion.type) {
                case "multiple":
                    answers.push(currentQuestion.correct_answer)
                    currentQuestion.incorrect_answers.forEach(a => {
                        answers.push(a)
                    })
                    dummyArray.push(2)
                    dummyArray.push(3)
                    break

                case "boolean":
                    answers.push(currentQuestion.correct_answer)
                    answers.push(currentQuestion.incorrect_answers[0])
                    break
            }

            // sort the answers in random order
            dummyArray = dummyArray.sort(() => Math.random() - 0.5)
            dummyArray.forEach(value => {
                answersRandomOrder.push(answers[value])
            })

            return answersRandomOrder
        }
    }
})